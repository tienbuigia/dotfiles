#!/bin/sh
#   ▄      ▀
# ▄▄█▄▄  ▄▄▄     ▄▄▄   ▄ ▄▄
#   █      █    █▀  █  █▀  █
#   █      █    █▀▀▀▀  █   █
#   ▀▄▄  ▄▄█▄▄  ▀█▄▄▀  █   █
#   this simple script just enable or disable the touchpad

if [ ! "$XDG_SESSION_TYPE" = "wayland" ]; then
  # Get the device ID of the touchpad
  DEVICE_ID=$(xinput list | grep -i "touchpad" | sed -n 's/.*id=\([0-9]*\).*/\1/p')

  [ -z "$DEVICE_ID" ] && exit 0 # exit if there's not any touchpad

  # Check if the touchpad is enabled or disabled
  STATUS=$(xinput list-props "$DEVICE_ID" | grep "Device Enabled" | awk '{print $NF}')

  if [ "$STATUS" = "1" ]; then
    # Disable the touchpad
    xinput --disable "$DEVICE_ID"
    notify-send "Touchpad disabled" -h string:x-canonical-private-synchronous:touchpadtoggle
  else
    # Enable the touchpad
    xinput --enable "$DEVICE_ID"
    notify-send "Touchpad enabled" -h string:x-canonical-private-synchronous:touchpadtoggle
  fi
else
  # for Hyprland specificly
  DEVICE_NAME="$(hyprctl devices | grep touchpad | tr -d '[:space:]')"
  STATUS="$(hyprctl getoption device:"$DEVICE_NAME":enabled | grep int | cut -d' ' -f2)"
  if [ "$STATUS" = "1" ]; then
    # Disable the touchpad
    hyprctl keyword device:name="$DEVICE_NAME":enabled false
    notify-send "Touchpad disabled" -h string:x-canonical-private-synchronous:touchpadtoggle
  else
    # Enable the touchpad
    hyprctl keyword device:name="$DEVICE_NAME":enabled true
    notify-send "Touchpad enabled" -h string:x-canonical-private-synchronous:touchpadtoggle
  fi
fi
