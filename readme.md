# My configuration repo

Welcome to this repo of mine, it contains all my configuration for the pieces
of software I use daily. Some of them I don't use anymore or rarely use, but I
still keep their configuration in case I try to use them again.

## Software I use daily

### Suckless suit

- WM: [dwm](https://codeberg.org/tienbuigia/dwm)
- Terminal: [st](https://codeberg.org/tienbuigia/st)
- Bar: [dwmblocks-async](https://codeberg.org/tienbuigia/dwmblocksa)
- Launcher: [dmenu](https://codeberg.org/tienbuigia/dmenu)
- Lock screen: [slock](https://tools.suckless.org/slock/)

### Others

- Editor: [neovim](https://neovim.io/), [helix](https://helix-editor.com/)
- Pdf reader: [zathura](https://github.com/pwmt/zathura)
- Browser: firefox with [arkenfox-user.js](https://github.com/arkenfox/user.js)
- Image viewer: [nsxiv](https://github.com/nsxiv/nsxiv)
- Key binds: [sxhkd](https://github.com/baskerville/sxhkd)
- Typing in vietnamese: [fcitx5](https://github.com/fcitx/fcitx5), [fcitx5-bamboo](https://github.com/fcitx/fcitx5-bamboo)
- Sound effect: [jamesdsp](https://github.com/Audio4Linux/JDSP4Linux)
- Music: [mpd](https://www.musicpd.org/) +
  [mpc](https://github.com/MusicPlayerDaemon/mpc) +
  [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp)
- Sound server: [pipewire](https://pipewire.org/) + pipewire-pulse
- Shell: [bash](https://www.gnu.org/software/bash/), [zsh](https://www.zsh.org/)
- Password manager: [pass](https://www.passwordstore.org/)
- Torrent: [transmission](https://github.com/transmission/transmission), [tremc](https://github.com/tremc/tremc)
- Terminal multiplexer: [tmux](https://github.com/tmux/tmux/wiki)
- Media player: [mpv](https://mpv.io/)
- Record, Webcam: [ffmpeg](https://github.com/FFmpeg/FFmpeg)
- Aur helper: [yay](https://github.com/Jguer/yay)
- Notification-daemon: [dunst](https://dunst-project.org/)
- Photo editor: [gimp](https://www.gimp.org/), [imagemagick](https://www.imagemagick.org/)
- Simple drawing: [mypaint](http://mypaint.org/)
- News, rss: [newsboat](https://github.com/newsboat/newsboat)
- Dotfiles manager: [yadm](https://yadm.io/)
- YouTube tool: [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- Netflix, Twitch player: [electronplayer](https://github.com/oscartbeaumont/ElectronPlayer)
- Messaging: [session](https://getsession.org/)
  - Xmpp client: [gajim](https://gajim.org/)
- Screenshot: [maim](https://github.com/naelstrof/maim)
- Fake keyboard & mouse: [xdotool](https://github.com/jordansissel/xdotool)

### Wayland Software

- WM: [dwl](https://github.com/djpohly/dwl), [Hyprland](https://wiki.hyprland.org/), [river](https://codeberg.org/river/river)
- Bar: [waybar](https://github.com/Alexays/Waybar)
- Terminal: [foot](https://codeberg.org/dnkl/foot), [kitty](https://sw.kovidgoyal.net/kitty/)
- Wallpaper: [swaybg](https://github.com/swaywm/swaybg)
- Lock screen: [swaylock](https://github.com/swaywm/swaylock)
- Screenshot/Colorpicker: [grim](https://sr.ht/~emersion/grim/)/[slurp](https://github.com/emersion/slurp)/[imagemagick](https://www.imagemagick.org/)
- Fake keyboard & mouse: [ydotool](https://github.com/ReimuNotMoe/ydotool)
