#!/bin/sh

info="$(riverctl list-inputs | grep -A1 -i 'touchpad')"
name="$(echo "$info" | head -n1)"
echo "$info" | grep 'true' >/dev/null && isConfigured=1

if [ "$isConfigured" != '1' ]; then
  riverctl input "$name" events disabled
else
  riverctl list-input-configs |
    grep -A1 "$name" |
    grep enabled >/dev/null &&
    riverctl input "$name" events disabled ||
    riverctl input "$name" events enabled
fi
