# vim:foldmethod=marker

# Bootstrap zcommet {{{
[ ! -f "${XDG_DATA_HOME:-${HOME}/.local/share}/zcomet/zcomet.zsh" ] && \
  command git clone --depth=1 "https://github.com/agkozak/zcomet.git" "${XDG_DATA_HOME:-${HOME}/.local/share}/zcomet"
# }}}

# Plugins {{{
source "${XDG_DATA_HOME:-${HOME}/.local/share}/zcomet/zcomet.zsh"

zcomet load agkozak/agkozak-zsh-prompt
zcomet load zsh-users/zsh-autosuggestions
# zcomet load zsh-users/zsh-syntax-highlighting
zcomet load zdharma-continuum/fast-syntax-highlighting
zcomet compinit
# }}}

# Foot output-pipe support {{{
precmd() { [[ ! -o zle ]] && print -n "\e]133;D\e\\" }
preexec() { print -n "\e]133;C\e\\" }
# }}}

# Zsh options {{{
PS1='%F{blue}%3~ %(?.%F{green}.%F{red})%#%f '

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'l:|=* r:|=*' 'r:|[._-]=* r:|=*'
zstyle ':completion:*' max-errors 3 numeric
bindkey -e

HISTFILE=$ZDOTDIR/.histfile
HISTSIZE=5000
SAVEHIST=5000

compdef _man nman
# }}}

# Others {{{
eval "$($HOME/.local/bin/mise activate zsh)"

[ -f "$HOME/.config/shell/aliases" ] && source "$HOME/.config/shell/aliases"
[ -f "$HOME/.config/shell/functions" ] && source "$HOME/.config/shell/functions"
# }}}
