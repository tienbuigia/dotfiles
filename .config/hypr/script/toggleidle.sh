#!/bin/sh
# toggle swayidle & send signal to waybar

pidof swayidle && killall swayidle || setsid -f swayidle

# shellcheck disable=2046 # intended split
kill -37 $(pidof waybar)
