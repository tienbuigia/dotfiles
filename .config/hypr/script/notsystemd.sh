#!/bin/sh
# start services without systemd

if ! pidof systemd; then
  stl='pipewire transmission-daemon mpd'
  for prog in $stl; do
    pgrep -nf "$prog" || setsid -f "$prog"
  done
fi
