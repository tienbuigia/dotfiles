#!/bin/sh

HYPRLAND_DEVICE="$1"

detect_touchpad() {
  hyprctl devices | grep 'touchpad' | tr -d '[:space:]'
}

[ -z "$HYPRLAND_DEVICE" ] && HYPRLAND_DEVICE="$(detect_touchpad)"

[ -z "$XDG_RUNTIME_DIR" ] && XDG_RUNTIME_DIR="/run/user/$(id -u)" &&
  export XDG_RUNTIME_DIR

export STATUS_FILE="$XDG_RUNTIME_DIR/touchpad.status"

enable_touchpad() {
  printf "%s\n" "true" > "$STATUS_FILE"
  notify-send "Enabling Touchpad"
  hyprctl keyword "device[$HYPRLAND_DEVICE]:enabled" true
}

disable_touchpad() {
  printf "%s\n" "false" > "$STATUS_FILE"
  notify-send "Disabling Touchpad"
  hyprctl keyword "device[$HYPRLAND_DEVICE]:enabled" false
}

if [ ! -f "$STATUS_FILE" ] || [ "$(cat "$STATUS_FILE")" = "false" ]; then
  enable_touchpad
else
  disable_touchpad
fi
