local options = {
    o = {
        title = true,
        number = true,
        relativenumber = true,
        termguicolors = true,
        colorcolumn = "80",
        cursorline = true,

        ignorecase = true,
        smartcase = true,
        hlsearch = true,

        wrap = true,
        breakindent = true,
        signcolumn = "yes",

        tabstop = 4,
        shiftwidth = 4,
        expandtab = true,
        softtabstop = 4,

        clipboard = "unnamedplus",
        undofile = true,

        completeopt = "menuone,noselect",
        showmode = false, -- since lualine already show the mode
    },
    g = {},
}

for key, val in pairs(options.o) do
    vim.opt[key] = val
end

for key, val in pairs(options.g) do
    vim.g[key] = val
end
