-- set filetype for my xresources files
vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
    pattern = { "*.Xresources", "*xresources*" },
    callback = function()
        vim.cmd("set ft=xdefaults")
    end,
})

-- auto switch fcitx5 mode for buffers
vim.api.nvim_create_autocmd({"InsertLeave"}, {
    callback = function ()
        local fcitx_status = tonumber(vim.fn.system('fcitx5-remote'))
        if fcitx_status == 2 then
            vim.b.fcitx_toggle_flag = true
            vim.fn.system("fcitx5-remote -c")
        end
    end
})
vim.api.nvim_create_autocmd({"InsertEnter"}, {
    callback = function ()
        if vim.b.fcitx_toggle_flag == nil then
            vim.b.fcitx_toggle_flag = false
        elseif vim.b.fcitx_toggle_flag then
            vim.fn.system("fcitx5-remote -o")
            vim.b.fcitx_toggle_flag = false
        end
    end
})
