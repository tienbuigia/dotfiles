return {
    "norcalli/nvim-colorizer.lua",
    name = "colorizer",
    config = function()
        vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
            callback = function()
                vim.cmd(":ColorizerAttachToBuffer")
            end,
        })
    end,
}
