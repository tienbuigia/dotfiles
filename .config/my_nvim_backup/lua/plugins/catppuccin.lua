return {
    "catppuccin/nvim",
    name = "catppuccin",
    opts = {
        flavour = "mocha",
        transparent_background = false,
        integrations = {
            cmp = true,
            gitsigns = true,
            dap = true,
            vimwiki = true,
            markdown = true,
        },
    },
}
