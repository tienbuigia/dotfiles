return {
    -- statusline
    "nvim-lualine/lualine.nvim",
    opts = {
        options = {
            icons_enabled = false,
            theme = "auto",
            component_separators = "|",
            section_separators = "",
        },
        sections = {
            lualine_c = {
                {
                    "buffers",
                    buffers_color = {
                        active = "lualine_b_active",
                        inactive = "lualine_c_inactive",
                    },
                },
            },
        },
    },
}
