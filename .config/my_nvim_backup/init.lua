require("options")

vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- install lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- install plugins
require("lazy").setup({
    -- tpope goodness & other old but good
    "tpope/vim-fugitive",
    "tpope/vim-surround",
    -- 'tpope/vim-commentary',
    "jkramer/vim-checkbox",

    { import = "plugins" },
})

require("lsp")
require("keymaps")
require("autocmd")
require("colorscheme")
